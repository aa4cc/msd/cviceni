clc;
clear;
%% Parameters:
L = 0.0015;
C1 = 1e-6;
C2 = 2e-6;
R1 = 100;
R2 = 20;

%% Input
i = 0;

num_of_sim = 3;
sim_time = 0.001;

uVoltage_time = (0:sim_time/100:sim_time)';


uVoltage{1} = sin(10000*uVoltage_time);
uVoltage{2} = ones(size(uVoltage_time));
uVoltage{3} = 5*ones(size(uVoltage_time));


%%
% init:
volt_data_out = cell(1,3);
volt_time_out = cell(1,3);

for ii = 1:num_of_sim
    uVoltageSig.time = uVoltage_time;
    uVoltageSig.signals.values = uVoltage{ii};
    uVoltageSig.signals.dimensions = 1;
    
    model_name = 'simscape_basics.slx';
    simOut = sim(model_name,'StartTime','0','StopTime',num2str(sim_time));
    volt_data_out{ii} = simOut.logsout.get('c2Voltage').Values.Data;
    volt_time_out{ii} = simOut.logsout.get('c2Voltage').Values.Time;
    % u{1} = ...
    % u{2} = ..
end

figure;
hold on;
for ii = 1:num_of_sim
    plot(volt_time_out{ii},volt_data_out{ii});
end




