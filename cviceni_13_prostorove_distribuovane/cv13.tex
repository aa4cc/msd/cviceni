\documentclass[a4paper,10pt]{scrartcl}
\usepackage[czech]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{placeins}

\usepackage[a4paper, total={7in, 9in}]{geometry}
\usepackage{amsmath}
\usepackage{cleveref}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{physics}
\usepackage{listings}
\usepackage{subcaption}
\usepackage{booktabs}
\usepackage{mathtools}
\usepackage{bm}


\usepackage{graphicx}
\usepackage{subcaption}
\usepackage{siunitx}
\usepackage{amssymb}
\usepackage{stmaryrd}
\usepackage{algpseudocode}



\newcommand{\mytitle}{MSD, Numerické řešení PDE}

\begin{document}
\title{\mytitle}
\author{Adam Uchytil, uchytada@fel.cvut.cz}
\date{2023}

\maketitle
\section{Úvod}
Chtěli bychom simulovat prostorově distribuovaný systém obecně dán parciální diferenciální rovnicí ve tvaru
\begin{equation*}
  \psi_t = F(\psi, \psi_{x}, \psi_{xx},\ldots,t),
\end{equation*}
jejímž řešením je funkce $\psi(x, t)$, kde $x$ je prostorová souřadnice a $t$ čas.
Taková rovnice může být například rovnice vedení tepla
\begin{equation*}
  \frac{\partial \psi}{\partial t} = \alpha\frac{\partial^2 \psi}{\partial x^2},
\end{equation*}
nebo třeba Burgersova rovnice modelující proudění
\begin{equation*}
  \frac{\partial \psi}{\partial t} =  \nu\frac{\partial^2 \psi}{\partial x^2}-\psi\frac{\partial \psi}{\partial x},
\end{equation*}
nebo dále také vlnová rovnice
\begin{equation*}
  \frac{\partial^2 \psi}{\partial t^2} = c^2\frac{\partial^2 \psi}{\partial x^2}
\end{equation*}
po uprávě na 
\begin{align*}
  \frac{\partial \psi_1}{\partial t} &= \psi_2,\\
  \frac{\partial \psi_2}{\partial t} &= c^2\frac{\partial^2 \psi_1}{\partial x^2}.
\end{align*}

% Taková rovnice se typicky řeší tak, že jí aproximujeme soustavou obyčejných diferencíálních rovnic, které (numericky) umíme řešit velmi dobře.
% Metody pro takovou aproximaci jsou například
% \begin{itemize}
%   \item Metoda konečných differencí (Finite Difference Method - FDM)
%   \item Metoda konečných objemů (Finite Volume Method - FVM)
%   \item Metoda konečných prvků (Finite Elemets Method - FEM)
% \end{itemize}
% Například v metodě konečných prvků vyjádříme řešení jako lineární kombinaci ortogonálních bázových prvků
\section{Method of Lines}
K simulaci přístoupíme pomocí tzv. Method of Lines. Řešení $\psi(x, t)$ diskretizujeme v prostoru a místo parciální diferenciální rovnice přejdeme k soustavě obyčejných diferenciálních rovnic.
Převod
\begin{equation*}
  \psi_t = F(\psi, \psi_{x}, \psi_{xx},\ldots,t) \to \dot{\tilde{\psi}}(t) = f(\tilde{\psi}(t), t),
\end{equation*}
kde 
\begin{equation*}
  \tilde{\psi}(t) = \begin{bmatrix}
    \psi(x_1, t) \\
    \psi(x_2, t)\\
    \vdots\\
    \psi(x_N, t)
  \end{bmatrix}.
\end{equation*}
Prostorové derivace v rovnici aproximujeme \emph{konečnými diferencemi}.

\section{Konečné diference}
Pro jednoduchost uvažujme pouze dva stupně volnosti a to prostorovou souřadnici $x$ a čas $t$.
Mějme funkci $\psi(x, t)$ a uvažujme její Taylorův rozvoj v okolí $x$
\begin{equation}
  \psi(x + \Delta x, t) = \psi(x, t) + \Delta x \frac{\partial \psi}{\partial x}\bigg\rvert_{x,t} + \frac{(\Delta x)^2}{2!}\frac{\partial^2 \psi}{\partial x^2}\bigg\rvert_{x,t} + \mathcal{O}((\Delta x)^3). \label{eqtaylor}
\end{equation}
Připomeňme si význam symbolu $\mathcal{O}((\Delta x)^n)$
\begin{equation}
  \lim_{\Delta x\to 0} \mathcal{O}((\Delta x)^n) \leq M (\Delta x)^n,
\end{equation}
pro nějaké nezáporné $M$.\\
Vyjádříme-li člen odpovídající derivaci v $x$ z \eqref{eqtaylor}, dostáváme
\begin{equation}
\frac{\psi(x + \Delta x, t) - \psi(x, t)}{\Delta x }  = \frac{\partial \psi}{\partial x}\bigg\rvert_{x,t} + \mathcal{O}(\Delta x). \label{eqforward}
\end{equation}
Této aproximaci derivace říkáme \emph{dopředná diference}. Pokud uvažujeme $-\Delta x$ místo $\Delta x$, dostaneme stejně rozumnou aproximaci
\begin{equation}
  \frac{\psi(x, t) - \psi(x - \Delta x, t)}{\Delta x }  = \frac{\partial \psi}{\partial x}\bigg\rvert_{x,t} + \mathcal{O}(\Delta x), \label{eqbackward}
\end{equation}
kterou nazýváme \emph{zpětná diference}. V neposlední řadě, pokud zprůměrujeme \eqref{eqforward} a \eqref{eqbackward}, dojde v Taylorově rozvoji k odečtení kvadratického členu a my tak dostáváme přesnější aproximaci
\begin{equation}
  \frac{\psi(x + \Delta x, t) - \psi(x - \Delta x, t)}{2\Delta x } = \frac{\partial \psi}{\partial x}\bigg\rvert_{x,t} + \mathcal{O}((\Delta x)^2),
\end{equation}
kterou nazýváme \emph{centrální diference}.\\
Jednoduchou aplikací odvozených schémat můžeme najít aproximaci druhé derivace
\begin{gather}
  \frac{1}{\Delta x} \left[ \frac{\psi(x + \Delta x, t) - \psi(x, t)}{\Delta x }  -  \frac{\psi(x, t) - \psi(x - \Delta x, t)}{\Delta x }\right] = \\
  \frac{\psi(x + \Delta x, t) - 2\psi(x, t) + \psi(x - \Delta x, t)}{(\Delta x )^2} = \frac{\partial^2 \psi}{\partial x^2}\bigg\rvert_{x,t} + \mathcal{O}((\Delta x)^2).
\end{gather}
Typicky uvažujeme pro daný čas $t$ místo funkce $\psi(x, t)$ pouze konečný počet hodnot $\psi_1, \psi_2, \ldots, \psi_n$ aproximující původní funkci v bodech $x_0, x_0 + \Delta x, \ldots, x_0 + (N-1)\Delta x$. To vede na značné zjednušení zápisu konečných diferencí a možnost pro jejich výpočet použít \textbf{matice a vektory}.
\subsection*{Příklad}
Chceme řešit následující jednodimenzionální zákon zachování
\begin{equation*}
  \frac{\partial \psi}{\partial t} = -c\frac{\partial \psi}{\partial x},
\end{equation*}
na množině $\Omega = \left[0, L\right]$ pro $t \in \left[0, T\right]$ se zadanou počáteční podmínkou $\psi(x, 0) = f(x)$.\\
Řešení budeme reprezentovat $N$ funkcemi času 
\begin{equation*}
  \psi_n(t) = \psi(x_n, t) \quad \text{pro} \quad n = 1,2,\ldots,N,
\end{equation*}
na rovnoměrné mřížce
\begin{equation*}
  x_n = (n-1)\Delta x \quad \text{pro} \quad n = 1,2,\ldots,N,
\end{equation*}
kde
\begin{equation*}
  \Delta x = \frac{L}{N-1}.
\end{equation*}
Aplikujeme metodu konečných diferencí. Pro první bod uvažujeme dopřednou diferenci, pro prostředních $N-2$ bodů centrální a pro poslední pak zpětnou. Tím získáme následující soustavu obyčejných diferencíálních rovnic
\begin{align*}
  \dot{\psi}_1(t) &= -c \frac{1}{\Delta x}\left[\psi_2(t) - \psi_1(t)\right],\\
  \dot{\psi}_n(t) &= -c \frac{1}{2\Delta x}\left[\psi_{n+1}(t) - \psi_{n-1}(t)\right] \quad \text{pro} \quad n  = 2,\ldots, N-1,\\
  \dot{\psi}_N(t) &= -c \frac{1}{\Delta x}\left[\psi_{N}(t) - \psi_{N-1}(t)\right].
\end{align*}
Diskretizaci lze dále přepsat do matico-vektorové podoby
\begin{equation*}
  \begin{bmatrix}
    \dot{\psi}_1\\
    \dot{\psi}_2\\
    \vdots\\
    \dot{\psi}_N
  \end{bmatrix} =-\frac{ c }{2\Delta x} \begin{bmatrix}
    -2 & 2 & 0 &  & \ldots &  & 0\\
    -1 & 0 & 1 &  & \ldots& & 0\\ 
    0 & -1 & 0 &  & \ldots & & 0\\ 
    \vdots & & & \ddots & & &  \vdots\\
    % 0 & \ddots & \ddots & \ddots & & \vdots\\
    % \vdots & 0 & -1 & 0 & 1 & 0 & \vdots\\
    % & & & \ddots & \ddots & \ddots &0\\
    0 &  & \ldots & & 0 & 1 &  0\\
    0 &  &\ldots & &  -1 & 0 & 1\\
    0 &  & \ldots& & 0 & -2 & 2\\
  \end{bmatrix}   \begin{bmatrix}
    \psi_1\\
    \psi_2\\
    \vdots\\
    \psi_n
  \end{bmatrix},
\end{equation*}
neboli
\begin{equation*}
  \dot{\bm{\psi}} = -c\bm{D}_x\bm{\psi}.
\end{equation*}
Například pro $ N = 5$
\begin{equation*}
  \bm{D}_x = \frac{1}{2\Delta x}\begin{bmatrix}
    -2 & 2 & 0 & 0 & 0\\
    -1 & 0 & 1 & 0 & 0\\
    0 & -1 & 0 & 1 & 0\\
    0 & 0 & -1 & 0 & 1\\
    0 & 0 & 0 &  -2 & 2
  \end{bmatrix}.
\end{equation*}
Odvozená obyčejná diferenciální rovnice má analytické řešení
\begin{equation*}
  \bm{\psi}(t) = \exp(-ct\bm{D}_x)\bm{f},
\end{equation*}
kde
\begin{equation*}
  \bm{f} = \begin{bmatrix}
    f(x_1)\\
    f(x_2)\\
    \vdots\\
    f(x_N)
  \end{bmatrix}.
\end{equation*}
\emph{Poznámka Proč jde o zákon zachování? Odvoďme si rovnici kontinuity}
\begin{align*}
  -\dot{m} &= \int_{\partial V} \rho \bm{v}\cdot \mathrm{d}\bm{S},\\
  \frac{\mathrm{d}}{\mathrm{d}t} \int_{V} \rho\;\mathrm{d}V &=  -\int_{\partial V} \rho \bm{v}\cdot \mathrm{d}\bm{S},\\
  \int_{V} \frac{\partial \rho}{\partial t}\;\mathrm{d}V &= -\int_{\partial V} \rho \bm{v}\cdot \mathrm{d}\bm{S},\\
  \int_{V} \frac{\partial \rho}{\partial t}\;\mathrm{d}V &= \int_{V} -\nabla \cdot (\rho\bm{v})\; \mathrm{d}V,\\
  \frac{\partial \rho}{\partial t} &= -\nabla \cdot (\rho\bm{v}).
\end{align*} 
\emph{Nyní pro 1D případ s $v = \text{const}$}
\begin{equation*}
  \frac{\partial \rho}{\partial t} = -v\frac{\partial \rho}{\partial x},
\end{equation*}
\emph{což je řešená rovnice pro $\psi = \rho$ a $c = v$.}

% na rovnoměrné mřížce $x_n = -L/2 + (n-1)\Delta x$, kde $x_n = -L/2 + (n-1)\Delta x$ pro $n = 1,2,\ldots,N$. Dále pak $\Delta x


\end{document}