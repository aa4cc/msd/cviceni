\documentclass[10pt]{article} % use larger type; default would be 10pt
\usepackage{graphicx}
\usepackage[utf8]{inputenc} % set input encoding (not needed with XeLaTeX)
\usepackage[czech]{babel}	
%Laplace
\usepackage{mathrsfs}
\usepackage{bbm}
\usepackage{physics}

\setlength{\voffset}{-0.65in}   %upper border
\setlength{\hoffset}{-0.65in}   %left border
\textwidth= 450pt
\textheight = 650pt

%\title{[B3M35LSY] Assignment}
\author{Loi Do, doloi@fel.cvut.cz}
\date{2020} % Activate to display a given date or no date (if empty),
         % otherwise the current date is printed

\usepackage{titlesec}

\titleformat*{\section}{\large\bfseries}
\titleformat*{\subsection}{\normalsize\bfseries}
\titleformat*{\subsubsection}{\normalsize\bfseries}
\titleformat*{\paragraph}{\normalsize\bfseries}
\titleformat*{\subparagraph}{\normalsize\bfseries}

\usepackage{xcolor}
\newcommand{\red}[1]{\textcolor{red}{#1}}
\newcommand{\blue}[1]{\textcolor{blue}{#1}}
\newcommand{\green}[1]{\textcolor{green}{#1}}
\newcommand{\orange}[1]{\textcolor{orange}{#1}}
\newcommand{\purple}[1]{\textcolor{purple}{#1}}

\usepackage{subcaption}
\usepackage{amsmath}
\usepackage{amssymb}
\title{B3B35MSD, 9. cvičení}
\begin{document}
\maketitle

Cílem tohoto cvičení je naučit se následující kompetence:

\section{Jakobiány}
Jakobiány lze vytvořit přímo na základě typů 'ramen' (zda se jedná o rotační, či posuvná ramena) a udávají vztah mezi jednotlivými absolutními rychlosti souřadných soustav, absolutními translačními rychlosti těžišť a zvolenými zob. souřadnicemi.
S Jakobiány se můžete seznámit také v doporučené literatuře:
\begin{itemize}
\item https://1lib.cz/book/3345739/50658a .
\end{itemize}
V knize se nicméně odvozuje tzv. 'Manipulator Jacobian', což je Jakobián pouze pro manipulátor (end-effector) na konci robotického ramene. 
Jakobián lze nicméně napsat pro libovolné rameno (jedná se vlastně o rozepsaný 'Manipulator Jacobian' na více částí).
Konstrukce Jakobiánu je vysvětlena v 5. kapitole (relevantní vztahy jsou od str. 108). 
V kapitole 5.6.1 je popsáno, jak se vytvářejí rotační jakobiány. Důležitý je vztah (5.54). 
Kapitola 5.6.2 pak popisuje konstrukci translačních jakobiánů.
Důležité vztahy jsou rovnice (5.66) a (5.78).

\subsection{Úloha na cvičení}
\begin{itemize}
\item Napište definiční vztah pro translační Jakobián těžiště $c_i$, tedy vztah mezi absolutní translační rychlostí  $v_{c_1}^0$ a vektorem zob. souřadnic $q$.
\item Napište definiční vztah pro rotační Jakobián $i$-tého souřadného systému, tedy vztah mezi absolutní rotační rychlostí $\omega_{01}^0$ a vektorem zob. souřadnic $q$.
\item Napište obecné vztahy pro výpočet rotačního jakobiánu zahrnující rotační i translační klouby jednotlivých ramen.
\item Napište obecné vztahy pro výpočet translačního jakobiánu zahrnující rotační i translační klouby jednotlivých ramen.
\item Napište rotační i translační jakobiány pro dual-stage kamerový systém z předchozího cvičení.
\end{itemize}
Výsledné vztahy (kromě posledního bodu) jsou pro kontrolu napsány níže u řešených příkladů.

\subsection{Několik řešených příkladů}

\subsubsection{Two-link manipulator}
\begin{figure}[h]
\centering
\includegraphics[width=0.7\textwidth]{figs/two_link_manipulator.pdf}
\caption{'Two-link cartesian manipulator'.}\label{fig:two_link_manipulator}
\end{figure}
Mějme robotické rameno: 'Two-Link Cartesian Manipulator' (kniha, str. 208), jehož schéma je na obr.~\ref{fig:two_link_manipulator} se zvolenými souřadnicovými systémy (volba je taková, aby výsledné vztahy souhlasili s řešením v knize). 
Souřadné soustavy 0 a 1 jsou v nákresu  pro přehlednost posunuty.
Rotační matice popisující transformace z jednotlivých souřadných soustav jsou:
\begin{equation}
R_1^0 = \begin{bmatrix}
1 & 0 & 0 \\
0 & 0 & 1 \\
0 &-1 & 0
\end{bmatrix}\;,
R_2^0 = R_1^0 \;.
\end{equation}
Při konstrukci translačních Jakobiánů nebudeme matice homogenní transformace potřebovat, jelikož v systému jsou pouze translační (prismatická) ramena.
Absence rotačních ramen taky implikuje nulovost rotačních jakobiánů.

Translační Jakobián $J_{c_i,\mathrm{TR}}$ těžiště $c_i$ je definován vztahem:
\begin{equation}\label{eq:jakobian_general}
v_{c_i}^0 = J_{c_i,\mathrm{TR}	} \dot{q}\;,
\end{equation}
kde $v_{c_i}^0$ je absolutní rychlost těžiště $c_i$ a $q$ je vektor zobecněných souřadnic:
\begin{equation}
q = \begin{bmatrix}
q_1 \\
q_2 \\
\vdots \\
q_N
\end{bmatrix}\;.
\end{equation}
Translační Jakobián lze obecně vypočítat následovně:
{\small
\begin{equation}\label{eq:trans_jakobian_vypocet}
J_{\red{c_i}, \mathrm{TR}} = 
\begin{bmatrix}
\overbrace{
\begin{cases}
\text{T}:	z_{\blue{0}}^0
\\
\text{R}:	z_{\blue{0}}^0 \times (r_{\red{c_i}}^0 - r_{\blue{0}}^0)
\end{cases}
}^{\text{\blue{1}. kloub}}
% ---
&
% ---
\hdots
% ---
&
% ---
\overbrace{
\begin{cases}
\text{T}:	z_{\blue{k}-1}^0
\\
\text{R}:	z_{\blue{k}-1}^0 \times (r_{\red{c_i}}^0 - r_{\blue{k}-1}^0)
\end{cases}
}^{\blue{k}-\text{tý kloub}}
% ---
&
% ---
\hdots
% ---
&
% ---
\overbrace{
\begin{cases}
\text{T}:	z_{\red{i}-1}^0
\\
\text{R}:	z_{\red{i}-1}^0 \times (r_{\red{c_i}}^0 - r_{\red{i}-1}^0)
\end{cases}
}^{\red{i}-\text{tý kloub}}
% ---
&
% ---
0
\end{bmatrix}\;.
\end{equation}}
Pakliže je $k$-tý kloub translační, platí první řádek matice uvozený písmenem $T$, a naopak platí druhý řádek matice uvozený písmenem $R$, pakliže je kloub rotační.
Všechny sloupce po $i$-tém sloupci jsou nulové.
Vektor $z_k^k = [0,0,1]^T$ (definiční vztah pro $z_k^k$ daný použitím DH notace), který lze následně transformovat do nulové souřadné soustavy:
\begin{equation}
z_k^0 = R_k^0 z_k^k \;, 
\end{equation}
Pozice těžiště $r_{c_i}^0$ a počátky souřadných soustav $r_{k}^0$ vůči absolutní souřadné soustavě se transformují pomocí homogenních transformací:
\begin{equation}
r_{c_i}^0 = T_i^0 r_{c_i}^i \;, r_{k}^0 = T_k^0 r_k^k\;.
\end{equation}

Vypočtěme nyní $J_{c_1,\mathrm{TR}}$ pro manipulátor.
První kloub (rameno) je translační, tedy:
\begin{equation}
J_{c_1,\mathrm{TR}} = \begin{bmatrix}
z_0^0 & \begin{bmatrix}
0 \\ 0 \\ 0
\end{bmatrix}
\end{bmatrix}
=
\begin{bmatrix}
0 & 0 \\
0 & 0 \\
1 & 0 
\end{bmatrix}\;.
\end{equation}
Druhý kloub je opět translační a tedy:
\begin{equation}
J_{c_2,\mathrm{TR}} = 
\begin{bmatrix}
z_0^0 & z_1^0
\end{bmatrix}
=
\begin{bmatrix}
z_0^0 & R_1^0 z_1^1
\end{bmatrix}
=
\begin{bmatrix}
0 & 0 \\
0 & 1 \\
1 & 0 
\end{bmatrix}\;.
\end{equation}
Při dosazení vypočtených Jakobiánů do rovnice~\eqref{eq:jakobian_general} a roznásobení dostáváme:
\begin{subequations}
\begin{align}
v_{c_1}^0  &= \begin{bmatrix}
0 & 0 \\
0 & 0 \\
1 & 0 
\end{bmatrix}
\begin{bmatrix}
\dot{q}_1 \\
\dot{q}_2 
\end{bmatrix}
= 
\begin{bmatrix}
0 \\
0 \\
\dot{q}_1
\end{bmatrix}\;,\\
%
v_{c_2}^0  &= \begin{bmatrix}
0 & 0 \\
0 & 1 \\
1 & 0 
\end{bmatrix}
\begin{bmatrix}
\dot{q}_1 \\
\dot{q}_2 
\end{bmatrix}
= 
\begin{bmatrix}
0 \\
\dot{q}_2 \\
\dot{q}_1
\end{bmatrix}\;.
\end{align}
\end{subequations}
Tyto vztahy odpovídají intuici. 
Translační rychlost těžiště $c_1$ v ose $z_0$ je rovna časové změně souřadnice $q_1$ a nezávisí na souřadnici $q_2$ (proto je druhý sloupec $J_{c_1,TR})$ nulový).
Translační rychlost těžiště $c_2$ je v ose $z_0$ stejná jako rychlost těžiště $c_1$, navíc se pohybuje v ose $y_0$ rychlostí $\dot{q}_2$.


\subsubsection{Dvojité kyvadlo}
\begin{figure}[h]
\centering
\includegraphics[width=0.5\textwidth]{figs/double_pendulum.eps}
\caption{Dvojité kyvadlo.}\label{fig:double_pendulum}
\end{figure}
Mějme model dvojitého kyvadla na obr.~\ref{fig:double_pendulum}. 
Obě těžiště jsou uprostřed daných ramen, obě ramena mají délku L.
Jako zobecněné souřadnice zvolíme $q = [\theta_1, \theta_2]^T$
Umístění souřadných soustav vede na následující rotační a homogenní transformace:
\begin{subequations}
\begin{align}
T_1^0 &= 
\begin{bmatrix}
\cos(\theta_1)	& -\sin(\theta_1)	&	0 &	L\cos(\theta_1) \\
\sin(\theta_1)	& \cos(\theta_1)	&	0 &	L\sin(\theta_1) \\
0				& 0					&	1 & 0				\\
0				& 0					&	0 & 1
\end{bmatrix}
\;, \\
T_2^0 &= 
\begin{bmatrix}
\cos(\theta_1 + \theta_2)	& -\sin(\theta_1 + \theta_2)	&	0 &	L\cos(\theta_1) + L\cos(\theta_1 + \theta_2) \\
%
\sin(\theta_1 + \theta_2)	& \cos(\theta_1 + \theta_2)	&	0 &	L\sin(\theta_1) + L\sin(\theta_1 + \theta_2) \\
r
0				& 0					&	1 & 0				\\
0				& 0					&	0 & 1
\end{bmatrix}\;.
\end{align}
\end{subequations}
Rotační Jakobián $J_{i,R}$ je definován následujícím vztahem:
\begin{equation}\label{eq:rot_jakobian_def}
\omega_{0i}^0 = J_{i,R} \dot{q} \;,
\end{equation} 
kde $\omega_{0i}^0$ je úhlová rychlost $i$-tého ramene.
Jakobián lze vypočítat následovně:
\begin{equation}
J_{i,R} = 
\begin{bmatrix}
\rho_1 z_0^0	&	\rho_2 z_1^0 	& \hdots	&  \rho_k z_{k-1}^0 & \hdots	&	\rho_i z_{i-1}^0 & 0 & \hdots & 0
\end{bmatrix} \;,
\end{equation}
kde:
\begin{equation}
\rho_k = 
	\begin{cases}
		0 &\text{Pokud je } k\text{-tý kloub translační} \\
		1 &\text{Pokud je } k\text{-tý kloub rotační} 
	\end{cases}
\end{equation}
pro $k = 1,2,\hdots, i\;$. 
Vektor $z_k^0$ je definován stejně jako v případě translačního jakobiánu.
Dosazením do vzorců dostáváme:
\begin{subequations}
\begin{align}
J_{1,R} &= 
\begin{bmatrix}
z_0^0 & \begin{bmatrix}
0 \\
0 \\ 
0
\end{bmatrix}
\end{bmatrix}
=
\begin{bmatrix}
0 & 0 \\
0 & 0 \\
1 & 0
\end{bmatrix} \;, \\
J_{2,R} &= 
\begin{bmatrix}
z_0^0 & z_1^0
\end{bmatrix} =
\begin{bmatrix}
z_0^0 & R_1^0z_1^1
\end{bmatrix}
=
\begin{bmatrix}
0 & 0 \\
0 & 0 \\
1 & 1
\end{bmatrix}\;.
\end{align}
\end{subequations}
Po dosazení Jakobiánů do rovnice \eqref{eq:rot_jakobian_def} dostáváme:
\begin{subequations}
\begin{align}
\omega_{01}^0 &= 
\begin{bmatrix}
0 \\
0 \\
\dot{q}_1
\end{bmatrix}\;,
\\
\omega_{02}^0 &= 
\begin{bmatrix}
0 \\
0 \\
\dot{q}_1 + \dot{q}_2
\end{bmatrix}\;,
\end{align}
\end{subequations}
což opět koresponduje s intuicí, že úhlová rychlost první ramene v ose $z$ je $\dot{q}_1$ a úhlová rychlost druhého ramene v ose $z$ je dána součtem $\dot{q}_1$ a $\dot{q}_2$.

Pro výpočet translačních Jakobiánů využijeme vzorec~\eqref{eq:trans_jakobian_vypocet} z předchozí úlohy.
Jelikož jsou oba klouby rotační, bude situace komplikovanější.
\begin{itemize}
\item 
Nejprve napišme pozici těžišť $c_i$ v vůči $i$-té souřadné soustavě, tedy vektory $r_{c_i}^i$:
\begin{subequations}
\begin{align}
r_{c_1}^1 &= 
\begin{bmatrix}
-L/2 \\
0 \\
0
\end{bmatrix}\;,
\\  
r_{c_2}^2 &=
\begin{bmatrix}
-L/2 \\
0 \\
0
\end{bmatrix}\;.
\end{align}
\end{subequations}
\item Pomocí homogenních transformací lze získat pozice těžišť vůči absolutní souřadné soustavě, tedy $r_{c_i}^0$ jako:
\begin{subequations}
\begin{align}
r_{c_1}^0 &= T_1^0r_{c_1}^1 = 
\begin{bmatrix}
\frac{L}{2} \cos(\theta_1) \\
\frac{L}{2} \sin(\theta_1) \\
0
\end{bmatrix}\;,
\\
r_{c_2}^0 &= T_2^0r_{c_2}^2 = 
\begin{bmatrix}
L\cos(\theta_1) + \frac{L}{2}\cos(\theta_1 + \theta_2)	\\
L\sin(\theta_1) + \frac{L}{2}\sin(\theta_1 + \theta_2)\\
0
\end{bmatrix}\;.
\end{align}
\end{subequations}
\item Pro konstrukci Jakobiánů jsou potřeba ještě absolutní polohy souřadných soustav $r_0^0$, ty lze získat opět pomocí homogenních transformací:
\begin{subequations}
\begin{align}
r_0^0 &= 
\begin{bmatrix}
0 \\ 0 \\ 0 
\end{bmatrix}\;,
\\
r_1^0 &= T_1^0
\begin{bmatrix}
0 \\ 0 \\ 0 
\end{bmatrix}
=
\begin{bmatrix}
L\cos(\theta_1) \\ L\sin(\theta_1) \\ 0
\end{bmatrix}\;.
\end{align}
\end{subequations}
\item Nyní lze již dosadit do vzorce \eqref{eq:trans_jakobian_vypocet}:
\begin{subequations}
\begin{align}
J_{c_1, TR} &= 
\begin{bmatrix}
-\frac{L}{2}\sin(\theta_1) & 0 \\
\frac{L}{2}\cos(\theta_1) & 0\\
0 & 0
\end{bmatrix} \;,
%
\\
%  
J_{c_2, TR} &= 
\begin{bmatrix}
-L\sin(\theta_1) - \frac{L}{2}\sin(\theta_1 + \theta_2) & -\frac{L}{2}\sin(\theta_1 + \theta_2) \\
L\cos(\theta_1)  + \frac{L}{2}\cos(\theta_1 + \theta_2) & \frac{L}{2}\cos(\theta_1 + \theta_2) \\
0 & 0
\end{bmatrix}\;.
\end{align}
\end{subequations}
\end{itemize}



% -------------------------------------
% -------------------------------------
% -------------------------------------


\section{Dynamika: Lagrangeova rovnice}
\subsection{Úloha na cvičení}
\begin{itemize}
\item Napište Lagrangeovu rovnici(-e) rovnou ve formátu nelineárního systému druhého řádu s využitím matice setrvačnosti $M$ (nebo také $D$), Coriolisovy matice $C$ a členu odpovídající vlivu gravitace~$g$  (resp. členu odpovídající potenciální energii, např. energie uložená v pružinách).
\item Napište vztahy pro výpočet matice $M$, $C$ a vlivu gravitace $g$.
\item Zamyslete nad významem jednotlivých matic. Zkuste nastínit 'odvození výpočtu' matice $M$. Vyjděte ze známého vztahu pro výpočet kinetické (ko-)energie $T^*$:
\begin{equation}
T^* = 
\frac{1}{2} (v_{c_i}^0)^Tm_iv_{c_i} + 
\frac{1}{2} (\omega_{0i}^0)^T I_i^0 \omega_{0i}^0\;,
\end{equation}
a zkuste zobecnit skalární hmotnost $m$ na matici $M$ s využitím vztahů pro Jakobiány~\eqref{eq:jakobian_general} a~\eqref{eq:rot_jakobian_def}.
\end{itemize}
Vztahy lze nalézt opět v již zmíněné knize v kapitole 9.3 (str. 205).
Pro kontrolu jsou vztahy vypsány i zde níže:


Lagrangeovu rovnici lze zapsat ve tvaru:
\begin{equation}
M(q)\ddot{q} + C(q,\dot{q})\dot{q} + g(q) = Q \;.
\end{equation}
kde $Q$ jsou externí síly působící na systém. Dále:
\begin{equation}
M(q) = \sum_{i=1}^N  
\left[ 
J_{c_i, \mathrm{TR}}^T m_iJ_{c_i, \mathrm{TR}} + J_{i, R}^T R_iI_i^iR_i^T J_{i,R}
\right]\;,
\end{equation}
kde $N$ je počet ramen, $m_i$ a $I_i^i$ je hmotnost, resp. moment setrvačnosti $i$-tého ramene.
$C(q,\dot{q})$ je matice, jejíž prvkyna pozici ($k$ - řádek, $j$- sloupec) jsou dány vztahem:
\begin{equation}
c_{kj} = \sum_{i=1}^N 
\left[
\frac{1}{2}
\left(
\pdv{M_{kj}}{q_i} + \pdv{M_{ki}}{q_j} - \pdv{M_{ij}}{q_k}
\right)
\dot{q}_i
\right]\;.
\end{equation}
Nakonec, člen odpovídající gravitaci lze vypočítat jako:
\begin{equation}
g(q) = \text{grad}_qV(q) = 
\begin{bmatrix}
\pdv{V(q)}{q_1} & \pdv{V(q)}{q_2} & \hdots & \pdv{V(q)}{q_k}\;
\end{bmatrix}^T
\end{equation}
kde $V(q)$ je potenciální energie systému.
Je-li v systému přítomna potenciální energie pouze skrz gravitační působení, lze vypočítat $V(q)$ jako:
\begin{equation}\label{eq:potential_energy}
	V(x) = \sum_{i=1}^N 
	\left[
		m_i 
		\mathbf{g} r_{c_i}^0
	\right]\;.	
\end{equation}
Ve vztahu~\eqref{eq:potential_energy} je zvýrazněno, že $\mathbf{g}$ je vektorem, který má jeden nenulový prvek rovnající se gravitačnímu zrychlení $g$.
To, na jaké pozici nenulový prvek je, záleží na orientaci nulté souřadné soustavy vůči gravitačnímu poli.
Například, směřuje-li osa $x_0$ směrem 'dolů', vektor gravitačního zrychlení bude mít tvar:
\begin{equation}
	\mathbf{g} = 
	\begin{bmatrix}
		-g & 0 & 0	
	\end{bmatrix}\;.
\end{equation}
\begin{itemize}
\item Nalezněte matice $M$ a $C$ pro systém z obr.~\ref{fig:two_link_manipulator} a případně i dvojité kyvadlo~\ref{fig:double_pendulum}. Výpočty proveďte v MATLABu. Zkuste kód psát tak, aby bylo možné výpočty provést i pro jiné (zadané) parametry systému (rotační matice, momenty setrvačnosti, Jakobiány). Toto se bude hodit pro řešení DÚ.
\end{itemize}


%Jako výchozí kód pro řešení tohoto úkolu můžete použít:
%\begin{lstlisting}
%g = 9.81;                               % m.s-1 graviracni zrychleni
%L = 1;                                  % m delka kyvadla
%
%%% TODO: Definujte nelinearni stavovy model za pomoci operatoru '@' (anonymni funkce)
%f = @...;      	 						% definice funkce
%
%%% TODO: Definujte pocatecni podminky a zacatek a konec simulace
%x0 = [pi/3; 0];                          % Pocatecni podminky
%tspan = [0 10];                         % [zacatek, konec] simulace
%
%%% TODO: Odsimulujte odezvu na pocatecni podminky pomoci funkce ode45
%[T,Y] = ode45(...);	
%
%plot(T,Y(:,1),'-',T,Y(:,2),'-.');
%xlabel('time [s]');
%legend('poloha','rychlost');
%grid on;
%
%\end{lstlisting}
%


%\begin{figure}[!h]
%\centering
%\includegraphics[width=0.6\textwidth]{figs/series_transformers.eps}
%\caption{Série transformátorů.}\label{fig:series_transformers}
%\end{figure}



% -------------------------------------
% -------------------------------------
% -------------------------------------



%\begin{figure}[!h]
%    \begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[width=0.65\textwidth]{figs/mech_trans.eps}
%        \caption{Vícestupňová převodovka (ozubená kolečka).}\label{fig:mech_trans}
%    \end{subfigure}
%    ~	
%    \begin{subfigure}[t]{0.5\textwidth}
%        \centering
%        \includegraphics[width=0.65\textwidth]{figs/electric_trans.eps}
%        \caption{Ideální elektrický tranformátor.}\label{fig:electric_trans}
%    \end{subfigure}%
%\caption{Příklady transformátorů z různých fyzikálních domén.}\label{fig:transformator}
%\end{figure}















\end{document}
