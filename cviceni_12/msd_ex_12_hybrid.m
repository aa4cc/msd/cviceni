clc;
clear;
%% Hydraulic tank
h_v = 10;
h_0 = 12;
Q_in = 0.5; 
Q_1out = 1;
Q_2out = 2;
A = 5;

%% Ball reflection

b = 0.1;
v_x0 = -3;
v_y0 = -2;
c = 0.8;