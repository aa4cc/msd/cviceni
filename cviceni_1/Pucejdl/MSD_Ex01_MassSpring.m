clear, clc;
%% Mass on spring
% We did not cover this example on Thursdays class. It is the second
% example in 'Exercise Problems' for week 1. It is dead simple.

% NOTE: Unlike the mathematical pendulum this system really is a (damped)
% harmonic oscillator.

% Questions to think about: Does the period depend on the amplitude of the
% oscillation? Does Earth' gravity affect such system? If so, then how?

% Define physical constants
k = 1; % Spring constant
b = 0.3; % damping
m = 1; % mass

A = [0,1;-k/m,-b/m];
B = [0;1];
C = [1,0];
D = 0;

% Example of constructing State-Space with named states and outputs 
sys = ss(A,B,C,D,...
    'statename',{'position' 'velocity'},...
    'outputname','position',...
    'inputname', 'force');

% Initial condition response
figure;
initial(sys,[1 0]);

% Step response
figure;
step(sys);

% Response to user input
figure;
t = linspace(0,10, 1000);
u = sin(t);
lsim(sys, u, t);

% !NOTE: Options to quickly simulate linear systems compared to
% nonlinear: (initial(), step(), lsim(), ...) vs. ode__(...).