clear, clc;
%% Simple pendulum

% physical constants
g = 9.81;
L = 1;

%odefun
% x = [x(1); x(2)]

f = @(t,x) [x(2); -g/L*sin(x(1))];

% Initial conditions and time interval.
% NOTE: with LTI system, only the width of the interval matters, not the 
% actual start time. The same trajectory of the solution do ODE is obtained
% for [0 10] as for e.g. [10 20].
x0 = [pi; 0];
tspan = [0 10];

% Solver function
[time, X] = ode45(f, tspan, x0);

% Plotting
h = plot(time, X(:,1), time, X(:,2), 'LineWidth', 2);
xlabel('time [s]')
legend('x1', 'x2')

%% Pendulum linearized

% State Space system matrices
A = [0 1; -g/L 0];
B = [0; 0];
C = [1, 0];
D = 0;

% State Space object definition
pend_ss = ss(A, B, C, D,...
    'statename', {'angle', 'velocity'},...
    'outputname', 'angle');

% Initial condition
x0 = [0 0.1];

% Simple option of simulating the initial condition response. The method
% with ode solver can also be used here instead.
initial(pend_ss, x0, 5)

%% Comparison of nonlin and lin systems


f = @(t,x) [x(2); -g/L*sin(x(1))];
f_lin = @(t,x) [x(2); -g/L*x(1)];


% try and change the initial conditions to observe the discrepancy between
% nonlinear and linear models
x0 = [0.1; 0];

tspan = [0 10];

[time, X] = ode45(f, tspan, x0);
[time_lin, X_lin] = ode45(f_lin, tspan, x0);

h = plot(time, X(:,1), time_lin, X_lin(:,1), ':', 'LineWidth', 2);
xlabel('time [s]')
legend('nelin', 'lin')