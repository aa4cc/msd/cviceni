clear, clc;
%% Simple electric circuit using DAE
% Circuit corresponds to the one from first homework, only the inductor is
% left out.

%Physical params
R = 1;
Cap = 0.1; % large capacitor :)

% DAE in matrix form
% Define matrices E, A and B
E = zeros(6,6);
E(1,1) = Cap;

A = zeros(6,6);
A(1,2) = 1;
A(2,5) = 1;
A(3,4) = -1;
A(3,6) = 1;
A(4,3) = 1;
A(4,4) = -R;
A(5,2) = -1;
A(5,4) = 1;
A(6,1) = 1;
A(6,3) = 1;
A(6,5) = -1;

B = zeros(6,1);
B(2,1) = -1;

%% Simulation with constant input (i.e. unit step input in t=0)

u = 1;

f = @(t,x) (A*x + B*u);
opts = odeset('Mass',E); % mandatory setup for DAE system
tspan = [0 1];
x0 = zeros(1,6);

% ode15s: https://www.mathworks.com/help/matlab/ref/ode15s.html
[T,Y] = ode15s(f,tspan, x0, opts);

% Plot
figure(1);
plot(T,Y(:,1:2), 'LineWidth',2);
hold on
plot(T, Y(:,3),'-.', 'LineWidth', 2);
legend('u_{C1}','i_{C1}','u_{R1}');
xlabel('Time [s]')


%% Alternative way to simulate step response
C = [1 0 0 0 0 0]; % select firtst state as an output
sysd = dss(A,B,C,0,E); % Descriptor state-space: https://www.mathworks.com/help/control/ref/dss.html

step(sysd)

%% Time-varying input
% Following code shows an efficient method for simulating systems with
% time-varying input. First method uses auxiliary function defined in the
% file myodefun.m for convenience. 

N = 1000; % Number of steps
tspan = linspace(0,5,N);
u = sin(tspan); % sine input
% u = [5*ones(1,N/2) 10*ones(1,N/2)]; % sequence of step inputs
opts = odeset('Mass',E);     % 
x0 = zeros(1,6);


%% Method 1: auxiliary odefun
[time,Y] = ode15s(@(t,x) myodefun(t,x,tspan,u, A, B), tspan, x0, opts);

% Plotting
figure;
plot(tspan,Y, 'LineWidth', 2);
legend('u_{C1}','i_{C1}','u_{R1}','i_{R1}','u_U','i_U');
xlabel('Time [s]')


%% Method 2: manual cyclic execution
Y = zeros(N, 6);
for i=2:N
    f = @(t,x) (A*x + B*u(i));
    curr_tspan = [tspan(i-1) tspan(i)];
    [T,out] = ode15s(f,curr_tspan, x0, opts);
    
    % Save output
    Y(i-1,:) = out(end,:);
    
    % Next iteration
    x0 = out(end,:);
end

% Plotting
figure;
plot(tspan,Y, 'LineWidth', 2);
legend('u_{C1}','i_{C1}','u_{R1}','i_{R1}','u_U','i_U');
xlabel('Time [s]')

%% Explicit DAE (Transform into State-Space)
% !NOTE: This method provides State-Space realization which ingores the
% physical structure of the problem.
% Resulting state may not correspond to expected state variables (capacitor\
% voltages/inductor currents). Use manual method to obtain better insight.

ss_realization = ss(sysd, 'explicit');
figure;
step(ss_realization);


%% Transformation of DAE into SS while maintaining the structure

% Split the vector of variables: x1 = x(1), x2 = x(2:end)

% Define the submatrices
A11 = A(1,1);
A12 = A(1,2:end);
A21 = A(2:end,1);
A22 = A(2:end, 2:end);

B1 = B(1,1);
B2 = B(2:end,1);

rank(A22) % Test A22 for regularity

A_new = E(1,1)\(A11 - A12/A22*A21);
B_new = E(1,1)\(-A12/A22*B2 + B1);

% Construct State-Space object
ss_new = ss(A_new,B_new,1,0);

% Plotting
figure;
step(ss_new);



