clc;
clear;
%% Priklad 2: Hmotnost na pruzine, linearni model
% 2.priklad hmotnost s pruzinou
k = 1;
b = 0.3;
m = 1;
A = [0,1;-k/m,-b/m]; B=[0;1];
C = [1,0]; D=0;
sys2 = ss(A,B,C,D,...
    'statename',{'poloha' 'rychlost'},...
    'outputname','poloha');

% Odezva na pocatecni podminky
figure;
initial(sys2,[1 0]);

% Odezva na jednodkovy skok
figure;
step(sys2);

% Odezva na libovolný vstup
figure;
t = linspace(0,10, 1000);
u = sin(t);
lsim(sys2, u, t);