clc;
clear;
%% Four rooms
%% Parameters
% nodes: distinct temperatures
% [1 ... 4]: room temperatures
% [5 ... 12]: inner wall temperatures
% [13 ... 20]: outer wall temperatures
% [21]: Outside temperature

nodes = [1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21];
num_nodes = numel(nodes);

Adj = zeros(num_nodes);
Adj(1,5) = 1;
Adj(1,9) = 1;
Adj(1,13) = 1;
Adj(2,6) = 1;
Adj(2,7) = 1;
Adj(2,15) = 1;
Adj(3,10) = 1;
Adj(3,11) = 1;
Adj(3,17) = 1;
Adj(4,8) = 1;
Adj(4,12) = 1;
Adj(4,19) = 1;
Adj(5,6) = 1;
Adj(7,8) = 1;
Adj(9,10) = 1;
Adj(11,12) = 1;
Adj(13,14) = 1;
Adj(14,21) = 1;
Adj(15,16) = 1;
Adj(16,21) = 1;
Adj(17,18) = 1;
Adj(18,21) = 1;
Adj(19,20) = 1;
Adj(20,21) = 1;

% Create bidirectional adjacency matrix
Adj = Adj+ Adj'; 
%Watch out - if there are ones on the diagonal (self adjacency), it would be
%necessary to subtract them once

                 
G = graph(Adj);
figure;
plot(G);
pause(0.1);
%% Thermal Capacitances
Cap = zeros(1,num_nodes);
Cap_room = 7.2e7;
C_wall_inner = 1e5;
C_wall_outer = 2e5;

Cap_outside = 1e50; % High capacitance effectively stands for hard temperature input
% Thermal capacitance of 'outside' (order e50 is reasonable)
% NOTE: Not a particularly nice way of doing this. Basically creates a stiff
% problem by inducing ultra slow dynamics of the outside temperature.

% Thermal capacitance of rooms (order e7 is reasonable)
Cap(1) = Cap_room;
Cap(2) = Cap_room;
Cap(3) = Cap_room;
Cap(4) = Cap_room;

% Thermal capacitance of 'outside'
Cap(21) = Cap_outside;   

% Inner walls of room 1
Cap(5) = C_wall_inner;
Cap(9) = C_wall_inner;
Cap(13) = C_wall_inner;

% Inner walls of room 2
Cap(6) = C_wall_inner;
Cap(7) = C_wall_inner;
Cap(15) = C_wall_inner;

% Inner walls of room 3
Cap(10) = C_wall_inner;
Cap(11) = C_wall_inner;
Cap(17) = C_wall_inner;

% Inner walls of room 4
Cap(8) = C_wall_inner;
Cap(12) = C_wall_inner;
Cap(19) = C_wall_inner;

Cap(14) = C_wall_outer;  % Outer walls of room 1
Cap(16) = C_wall_outer;  % Outer walls of room 2
Cap(18) = C_wall_outer;  % Outer walls of room 3
Cap(20) = C_wall_outer;  % Outer walls of room 4

%% Thermal Resistances
Res = zeros(num_nodes);
R_inner = 1/715;
R_mid = 1/3516;
R_outer = 1/1430;

% Inner walls of room 1
Res(1,5) = R_inner;
Res(1,9) = R_inner;
Res(1,13) = R_inner;

% Inner walls of room 2
Res(2,6) = R_inner;
Res(2,7) = R_inner;
Res(2,15) = R_inner;

% Inner walls of room 3
Res(3,10) = R_inner;
Res(3,11) = R_inner;
Res(3,17) = R_inner;

% Inner walls of room 4
Res(4,8) = R_inner;
Res(4,12) = R_inner;
Res(4,19) = R_inner;

% Mid walls
Res(5,6) = R_mid;
Res(7,8) = R_mid;
Res(9,10) = R_mid;
Res(11,12) = R_mid;
Res(13,14) = R_mid;
Res(15,16) = R_mid;
Res(17,18) = R_mid;
Res(19,20) = R_mid;


Res(14,21) = R_outer;   % Outer walls of room 1
Res(16,21) = R_outer;   % Outer walls of room 2
Res(18,21) = R_outer;   % Outer walls of room 3
Res(20,21) = R_outer;   % Outer walls of room 4

% Make the matrix symmetric
Res = (Res + Res')/2;

%% System matrix, could be created more efficiently

A = zeros(num_nodes);

% Off-diagonal terms in A
for ii=1:num_nodes
    for jj=1:num_nodes
        if(ii ~= jj && Adj(ii,jj) == 1)
            A(ii,jj) = 1/(Cap(ii)*Res(ii,jj));
        end
    end
end

% Diagonal terms in A
for ii=1:num_nodes
    for jj=1:num_nodes
        if(ii == jj)
            A(ii,jj) = -sum(A(ii,:));
        end
    end
end


%% State space
B = zeros(num_nodes,1); % Input matrix
B(1) = 0.0006;            % Add heat flux input to room 1

% Observe only room temperatures
C = zeros(4,21);    % Output matrix
C(1,1) = 1;
C(2,2) = 1;
C(3,3) = 1;
C(4,4) = 1;

D = 0;
room4_sys = ss(A,B,C,D);

% Initial temperatures
T0 = 10*ones(num_nodes,1);      % Initial temperature of walls

% Initial temperature of rooms, temperatures T1, T2, T3 and T4
T0(1) = 30;
T0(2) = 20;
T0(3) = 25;
T0(4) = 15;

% Outside temperature
Tout = -10;
T0(21) = Tout;


% Run the simulation at 100 samples for some number of hours
hrs = 100;
t = linspace(0,hrs*3600,100);   % Simulate for 'hrs' hours

y = lsim(room4_sys,ones(size(t)),t,T0);

X = [1*ones(4,1), 2*ones(4,1)-eps, 2*ones(4,1)+eps, 3*ones(4,1)];
Y = X';

%% PLOT 3D visualization
figure;

% Map the room layout to what we did in the example
a = 3;
b = 4;
c = 1;
d = 2;
% change default colormap for nicer surface plot
colormap(jet)

for ii=1:numel(t)
    Z = [y(ii,a), y(ii,a), y(ii,b), y(ii,b);
        y(ii,a), y(ii,a), y(ii,b), y(ii,b);
        y(ii,c), y(ii,c), y(ii,d), y(ii,d);
        y(ii,c), y(ii,c), y(ii,d), y(ii,d)];
    ax1 = surf(X,Y,Z);
    
    txt(1) = text(1.5,1.5,y(ii,a)+1.5,'\Theta_3','FontWeight', 'bold');
    txt(2) = text(2.5,1.5,y(ii,b)+1.5,'\Theta_4','FontWeight', 'bold');
    txt(3) = text(1.5,2.5,y(ii,c)+1.5,'\Theta_1','FontWeight', 'bold');
    txt(4) = text(2.5,2.5,y(ii,d)+1.5,'\Theta_2','FontWeight', 'bold');
    zlim([min(T0) max(T0)+15]);
    caxis([min(T0) max(T0)]);
    set(gca,'xticklabel',{[]})
    set(gca,'yticklabel',{[]})
    zlabel('Room temperature [^{\circ}C]')
    colorbar;
    %     view(0,90);
    title(['Time: ',num2str(round(t(ii)/3600)),' hours, outside temperature ',num2str(Tout),' ^{\circ}C'])
    drawnow;
    
    if ii == 1
        pause;
    end
end

