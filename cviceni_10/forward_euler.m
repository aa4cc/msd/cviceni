function [t, x] = forward_euler(F,tspan, x0, h)
%forward_euler First order method for numerical simulation
% F: Function to be solved
% tspan: [start_time, end_time]
% x0: initial conditions: column vector
% h: integration step
% 
% t: column vector: times of integration
% x: approximate solution of the F at times time_out

t0 = tspan(1); 
t_f = tspan(2); % Final time

% Preallocation of the memory and initialization
t = (t0:h:t_f+h)';                     % Define time_out vector
x = zeros(numel(t), numel(x0));  % Row: integration step, Column: values of the states
x(1,:) = x0';

k = 1;
while t(k) < t_f
    dot_x_k = F(t(k),x(k,:)');   % Evaluate dot x, note that F takes time as first input. No need to save this temporary value
    x(k+1,:) = (x(k,:) + h*dot_x_k');   % There must be a transposition to match the dimensions
%     t(k+1) = t(k) + h;      % Redundant
    % Next iteration:
    k = k + 1;
end

