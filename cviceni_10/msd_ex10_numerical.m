clc;
clear;
% Requirements: Optimization toolbox for Backward Euler

%%
F = @(t,y) [0 1; -1 0]*y;            % Second order system: harmonic oscillator: pendulum
tspan = [0 5*pi];
x0 = [1;0]; % [angle, speed]
h = 0.01;


% profile on; % Performance evaluation
[t_ode45,x_ode45] = ode45(F,tspan,[1; 0]);         % Numerical solution using ode45
% opts = odeset('RelTol',1e-4,'AbsTol',1e-6);   % Optional
% [t,y] = ode45(F,[0 5*pi],[1; 0], opts);

% Forward Euler
[t_FE, x_FE] = forward_euler(F,tspan, x0, h);

% Backward Euler
[t_BE, x_BE] = backward_euler(F,tspan, x0, h);
% profile off;
% profile viewer;
%%
figure;
plot(t_ode45, x_ode45(:,1), 'Linewidth', 2);
hold on;
plot(t_FE, x_FE(:,1), 'Linewidth', 2, 'Color', 'r');
plot(t_BE, x_BE(:,1), 'Linewidth', 2, 'Color', 'g');
legend('Ode45, angle', 'FE, angle', 'BE, angle');
grid on;

%% Stiff system solving
tspan = [0 1000];
% tspan = [0 3000];

x0 = [2;0];
h = 0.1;

[t_ode15s,y_ode15s] = ode15s(@vdp1000,tspan,x0);
tic;
[t_ode45,y_ode45] = ode45(@vdp1000,tspan,x0);  
toc;
% [t_FE, x_FE] = forward_euler(@vdp1000,tspan, x0, h);
% [t_BE, x_BE] = backward_euler(@vdp1000,tspan, x0, h);

figure;
plot(t_ode15s,y_ode15s(:,1),'-o');

figure;
plot(t_ode45,y_ode45(:,1),'-o');
title('Solution of van der Pol Equation, \mu = 1000');
xlabel('Time t');
ylabel('Solution y_1');

%% Funkce se vstupem
u = @(t) sin(t);
F = @(t,x) -10*x + x^2 + u(t); 

% ...


function dydt = vdp1000(t,y)
%VDP1000  Evaluate the van der Pol ODEs for mu = 1000.
%
%   See also ODE15S, ODE23S, ODE23T, ODE23TB.

%   Jacek Kierzenka and Lawrence F. Shampine
%   Copyright 1984-2014 The MathWorks, Inc.

dydt = [y(2); 1000*(1-y(1)^2)*y(2)-y(1)];
end
